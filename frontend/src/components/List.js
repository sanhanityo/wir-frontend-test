import React from 'react';
import { EditButton } from './Button';

const List = (props) => {
    const { contact } = props;
    const initial = contact.name.substring(0, 1)
    return(
        <div className="list">
            <div className="list__avatar">
                {initial}
            </div>
            <div className="list__info">
                <h3 className="list__info-name">{contact.name}</h3>
                <p className="list__info-phone">{contact.phone}</p>
            </div>
            <div className="list__action">
                <EditButton id={contact.id}/>
            </div>
        </div>
    )
}

export default List;