import React from 'react';
import {
    Link
} from "react-router-dom";

import AddIcon from "../assets/add.svg";
import EditIcon from "../assets/edit.svg";

const AddButton = () => (
    <Link className="btn-add" to="/add">
        <img src={AddIcon} alt=""/>
    </Link>
)

const EditButton = (props) => (
    <Link className="btn-edit" to={`/edit/${props.id}`}>
        <img src={EditIcon} alt=""/>        
    </Link>
)

export {
    AddButton,
    EditButton
}