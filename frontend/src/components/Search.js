import React from "react";

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            reset: false,
            search: ''
        }
        this.changeVal = this.changeVal.bind(this);
        this.enterVal = this.enterVal.bind(this);
        this.resetSearch = this.resetSearch.bind(this);
    }
    
    changeVal(event) {
        const val = event.target.value
        const reset = val.length > 0 ? true : false;
        this.setState({
            reset,
            search: val
        })
    }

    enterVal(event) {
        if (event.key === 'Enter') {
            const { search } = this.state
            this.props.actions(search)
        }
    }

    resetSearch() {
        this.props.reset()
        this.setState({search: '', reset: false})
    }

    render() {
        const { reset, search } = this.state
        return(
            <React.Fragment>
                <div className="container">
                    <div className="search">
                        <input className="search__input" placeholder="Search Contact" value={search} onChange={this.changeVal} onKeyPress={this.enterVal}/>
                        {
                            reset ? 
                                <button className="search__reset" onClick={this.resetSearch}>&times;</button> : ''
                        }
                    </div>
                </div>
                <div className="line" />
            </React.Fragment>
        )
    }
}

export default Search