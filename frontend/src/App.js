import React from 'react';
import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";
import {
  Home,
  AddContainer,
  EditContainer
} from "./container";
import './App.css';

function App() {
  return (
    <div className="main">
      <Router>
        <Route exact path="/" component={Home} />
        <Route exact path="/add" component={AddContainer} />
        <Route exact path="/edit/:id" component={EditContainer} />
      </Router>
    </div>
  );
}

export default App;
