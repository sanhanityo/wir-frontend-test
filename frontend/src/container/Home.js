import React from 'react';
import Axios from 'axios';
import Search from "../components/Search";
import { AddButton } from "../components/Button";
import List from "../components/List";
import Heading from '../components/Heading';

class Home extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            'contacts': []
        }
        this.fetch = this.fetch.bind(this)
        this.searching = this.searching.bind(this)
    }

    componentDidMount() {
        this.fetch()
    }

    fetch() {
        Axios.get('http://localhost:3001').then(res => {
            const contacts = res.data.data
            this.setState({
                contacts
            });
        })
    }

    searching(value) {
        Axios.post('http://localhost:3001/search', {q: value}).then(res => {
            const contacts = res.data.data
            // console.log(res.data)
            this.setState({
                contacts
            });
        })
    }
    render() {
        const { contacts } = this.state
        return (
            <React.Fragment>
                <Heading title="Contact List"/>
                <Search actions={this.searching} reset={this.fetch}/>
                <div className="list-wrapper">
                    {
                        contacts.map((contact, i) => 
                            <List contact={contact} key={i}/>
                        )
                    }
                </div>
                <AddButton />
            </React.Fragment>
        )
    }
}

export default Home