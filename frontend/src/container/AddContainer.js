import React from 'react';
import {
    Formik,
    Field,
    Form,
    ErrorMessage
} from 'formik';
import * as Yup from 'yup';
import Axios from 'axios';
import Heading from '../components/Heading';

class AddContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            phone: '',
            address: '',
            message: false
        }
    }

    render() {
        const { name, phone, address, message } = this.state
        return (
            <React.Fragment>
                <Heading title="Add Contact" nav/>
                {
                    message ? <div class="success">Success add data</div> : ''
                }
                <Formik
                    enableReinitialize
                    initialValues={{name, phone, address}}
                    validationSchema = {
                        Yup.object().shape({
                            name: Yup.string()
                                .required('Name Required'),
                            phone: Yup.string()
                                .required('Phone Required'),
                            address: Yup.string()
                                .required('Address Required'),
                        })
                    }
                    onSubmit={(values, actions) => {
                        this.setState({ message: false })
                        Axios.post(`http://localhost:3001/store`, {
                            name: values.name,
                            phone: values.phone,
                            address: values.address,
                        }).then(res => {
                            if(res.data.message === 'success') {
                                this.setState({ message: true })
                            }
                        })
                    }}
                    render={({ errors, status, touched, isSubmitting }) => (
                        <div className="container">
                            <Form>
                                {status && status.msg && <div>{status.msg}</div>}
                                <div className="form-group">
                                    <label className="form-label">Name</label>
                                    <Field type="text" name="name" className="form-input"/>
                                    <ErrorMessage name="name" className="form-error" component="div" />  
                                </div>
                                <div className="form-group">
                                    <label className="form-label">Phone</label>
                                    <Field type="text" name="phone" className="form-input"/>
                                    <ErrorMessage name="phone" className="form-error" component="div"/>  
                                </div>
                                <div className="form-group">
                                    <label className="form-label">Address</label>
                                    <Field component="textarea" name="address" className="form-input"/>
                                    <ErrorMessage name="address" className="form-error" component="div"/>  
                                </div>
                                <button type="submit" className="btn-action">SAVE</button>
                            </Form>
                        </div>
                    )}
                />
            </React.Fragment>
        )
    }
}

export default AddContainer