const sqlite3 = require('sqlite3').verbose()
const faker = require('faker');

const DBSOURCE = "db.sqlite"

let db = new sqlite3.Database(DBSOURCE, (err) => {
    if (err) {
        console.error(err.message)
        throw err
    } else {
        console.log('Connected to the SQLite database.')
        db.run(`CREATE TABLE contacts (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                name text, 
                phone text,
                address text
            )`,
            (err) => {
                if (err) {
                    // Table already created
                } else {
                    // Table just created, creating some rows
                    const insert = 'INSERT INTO contacts (name, phone, address) VALUES (?,?,?)';
                    for (let index = 0; index < 10; index++) {
                        let address = faker.address.streetAddress("###") + faker.address.city() + faker.address.state() + faker.address.county()
                        db.run(insert, [faker.name.findName(), faker.phone.phoneNumberFormat(0), address])
                    }
                }
            });
    }
});

module.exports = db
