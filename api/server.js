const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser')
const db = require('./database.js');
const HTTP_PORT = '3001';

const corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200 
};

app.use(cors(corsOptions));
app.use(bodyParser.json())

app.get('/', (req, res, next) => {
    const sql = "select * from contacts ORDER BY name ASC";
    const params = [];

    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({
                "error": err.message
            });
            return;
        }
        res.json({
            "message": "success",
            "data": rows
        })
    });
})

app.post("/store", (req, res, next) => {
    const errors = []
    if (!req.body.name) {
        errors.push("No name specified");
    }
    if (!req.body.phone) {
        errors.push("No phone specified");
    }
    if (!req.body.address) {
        errors.push("No address specified");
    }
    if (errors.length) {
        res.status(400).json({
            "error": errors.join(",")
        });
        return;
    }
    const data = {
        name: req.body.name,
        phone: req.body.phone,
        address: req.body.address
    }
    const sql = 'INSERT INTO contacts (name, phone, address) VALUES (?,?,?)'
    const params = [data.name, data.phone, data.address]
    db.run(sql, params, function (err, result) {
        if (err) {
            res.status(400).json({
                "error": err.message
            })
            return;
        }
        res.json({
            "message": "success",
            "data": data
        })
    });
})

app.get("/detail/:id", (req, res, next) => {
    const sql = "select * from contacts where id = ?"
    const params = [req.params.id]
    db.get(sql, params, (err, row) => {
        if (err) {
            res.status(400).json({
                "error": err.message
            });
            return;
        }
        res.json({
            "message": "success",
            "data": row
        })
    });
});

app.post("/update/:id", (req, res, next) => {
    const data = {
        name: req.body.name,
        phone: req.body.phone,
        address: req.body.address
    }
    db.run(`UPDATE contacts set 
            name = ?, 
            phone = ?,
            address = ?
            WHERE id = ?`,
        [data.name, data.phone, data.address, req.params.id],
        function (err, result) {
            if (err) {
                console.log(err);
                res.status(400).json({
                    "error": res.message
                })
                return;
            }
            res.json({
                message: "success",
                data: data,
                changes: this.changes
            })
        });
})

app.post("/search", (req, res, next) => {
    const data = {
        q: req.body.q
    };
    const params = [];
    const sql = `SELECT * FROM contacts WHERE name LIKE '%${data.q}%' OR phone LIKE '%${data.q}%'`;
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({
                "error": err.message
            });
            return;
        }
        res.json({
            "message": "success",
            "data": rows
        })
    });
})

app.listen(HTTP_PORT, () => {
    console.log("Server running on port %PORT%".replace("%PORT%", HTTP_PORT))
});