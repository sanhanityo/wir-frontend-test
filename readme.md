Technical Test FE React JS for WIR Group. [Demo](http://128.199.190.177:3000/)

## Project Structure
```
 API
    database.js
    db.sqlite
    package.json
    server.js
 Frontend
    public
    src
        components
        container
        App.js
        App.css
        index.js
    package.json
    server.js
```

## Installation
### Api
```
$ cd api
$ npm install
$ npm start
```
this project will run at port 3001, ex: http://localhost:3001

### Frontend
```
$ cd frontend
$ npm install
$ npm start
```
this project will run at port 3000, ex http://localhost:3000

## Library
- Express.js
- cors
- body parser
- sqlite3
- React.js
- React Router
- Formik
- Axios